#!/usr/bin/env bash
# backup.sh:
#   Back up CA and VPN nodes; sends backup alerts to email
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#______________________________________________________________________________

###############################################################################
# Global variables/constants
#------------------------------------------------------------------------------
# SSH addresses
CA_NODE_ADDRESS='caNodeAddress2Replace'
VPN_NODE_ADDRESS='vpnNodeAddress2Replace'
# Email
ALERT_EMAIL='alertEmail2Replace'
ALERT_EMAIL_PASSWORD='alertEmailPassword2Replace'
# Env
ADMIN_USERNAME='admin'
# Directories
BACKUP_DIR="/home/${ADMIN_USERNAME}/backup_dir"


###############################################################################
# Functions
#------------------------------------------------------------------------------
function Rsync_Command {
    local args="${*:-}"
    # shellcheck disable=SC2086
    rsync \
      --archive \
      --compress \
      --partial \
      --copy-links \
      --recursive \
      --rsh 'ssh -p 2222 -i /home/admin/.ssh/id_rsa -o "StrictHostKeyChecking=no"' \
      $args
}

function Curl_Mail_Command {
    local args="${*:-}"
    curl \
      --silent \
      --ssl-reqd \
      --url 'smtps://smtp.gmail.com:465' \
      --user "${ALERT_EMAIL}:${ALERT_EMAIL_PASSWORD}" \
      --mail-from "$ALERT_EMAIL" \
      --mail-rcpt "$ALERT_EMAIL" \
      --upload-file \
      "$args"
}

function Send_Backup_Size_To_Email {
    local smaller_msg='Subject: Backup Alert\n\nBackup is smaller than expected!'
    local excepted_size='552'  # KB
    local actual_size
    actual_size="$(du --summarize $BACKUP_DIR | cut --fields 1)"

    if (( actual_size < excepted_size )); then
        Curl_Mail_Command <(echo -e "$smaller_msg")
    fi
}

function Send_Bad_Status_Code_To_Email {
    local status_code="$1"
    local msg="Subject: Backup Alert\n\nBackup failed!\nStatus code: ${status_code}"

    [[ "$status_code" -ne 0 ]] && Curl_Mail_Command <(echo -e "$msg")
}


###############################################################################
# Main code
#------------------------------------------------------------------------------ 
Rsync_Command "$CA_NODE_ADDRESS":/home/admin/easy-rsa \
  /home/admin/backup_dir/CA_node/
Send_Bad_Status_Code_To_Email "$?"

Rsync_Command "$VPN_NODE_ADDRESS":/home/admin/easy-rsa \
  /home/admin/backup_dir/VPN_node/
Send_Bad_Status_Code_To_Email "$?"

Rsync_Command "$VPN_NODE_ADDRESS":/home/admin/OpenVPN-clients \
  /home/admin/backup_dir/VPN_node/
Send_Bad_Status_Code_To_Email "$?"


Send_Backup_Size_To_Email

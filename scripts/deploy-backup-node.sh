#!/usr/bin/env bash
# deploy-backup-node.sh:
#   Deploys and configure backup node (rsync as backup tool)
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#______________________________________________________________________________

###############################################################################
# Error Handling
#------------------------------------------------------------------------------
# A handler function for terminating signals and errors,
# which should clean up after itself
#   Usage: Error_Handler
#   Global vars:      no
#   Input parameters: no
#   Output values:    no
function Error_Handler {
    local exit_code="$?"

    echo "Error: exit code: $exit_code" >&2

    echo 'Disabling the net configuration...' >&2
    iptables --flush

    # Removing created directories
    for dir in "$CA_NODE_BACKUP_DIR" "$VPN_NODE_BACKUP_DIR"; do
       [[ -d "$dir" ]] && rm --recursive --for "$dir"
    done

    # Removing rsync systemd unit and other associated files
    dpkg --status "rsync-backup" &> /dev/null && apt purge --yes rsync-backup

    if systemctl list-units --all | grep --quiet "rsync-backup.service"; then
        systemctl stop rsync-backup.service

        for systemd_unit in 'rsync-backup.service' 'rsync-backup.timer'; do
          systemctl is-enabled "$systemd_unit" &> /dev/null \
            && systemctl disable "$systemd_unit"
        done

        systemctl daemon-reload
    fi

    [[ -f "$RSYNC_SCRIPT_FILE" ]] && rm --force "$RSYNC_SCRIPT_FILE"


    # Stopping containers if they running
    if docker ps | grep --quiet 'node-exporter'; then
        echo "Stoping all containers..."
        docker compose --file "$COMPOSE_FILE" down
    fi

    exit "$exit_code"
}  # End of Error_Handler function


# Unofficial strict mode
#   -e  --  Ends the script on the first error
#   -E  --  Allows the trap command to work
#   -u  --  When trying to access undefined var the script terminates
#   -o pipefail  -- Terminates the script if any command in the pipeline fails
set -eEuo pipefail

# A signal trap that calls the Error_Handler function
#   ABRT - abort interrupt signal
#   HUP  - terminal restart signal
#   INT  - interrupt signal (Ctrl+C)
#   QUIT - exit signal (Ctrl+C)
#   TERM - termination signal (allows the process to terminate gracefully)
#   ERR  - error signal
trap Error_Handler ABRT HUP INT QUIT TERM ERR

# Outputs an error (in STDERR) and terminates the script
# if the script is run without superuser rights
#   About using bash idioms
#       (source: "bash Idioms. Carl Albing & JP Vossen" book)
#       The bash idiom is used instead of the `if` operator,
#       because `if` operator for the case with 1-2 commands
#       in the then/else branch is too large and unwieldy.
(( EUID != 0 )) && { echo 'The script needs superuser rights.' >&2; exit 1; }


###############################################################################
# Global variables/constants
#------------------------------------------------------------------------------
# Sourcing environment variables from env-file
source /home/admin/cua-infrastructure-setup/scripts/set-env.sh
# Getting the IP from which you are currently connected via ssh
# (that is, administration node) for iptables rules configuration
ADMIN_NODE_IP="$(
    who --ips | grep "$ADMIN_USERNAME" | awk '{print $5}' | sort --unique
)"
# Deployment environment information
DOCKER_NETWORK_NAME='bck-docker-net'
NET_INTERFACE_NAME="$(ip route | grep 'default' | awk '{print $5}')"
# Backup directories
CA_NODE_BACKUP_DIR="/home/${ADMIN_USERNAME}/backup_dir/CA_node"
VPN_NODE_BACKUP_DIR="/home/${ADMIN_USERNAME}/backup_dir/VPN_node"
# Files
RSYNC_SCRIPT_FILE='/usr/local/bin/backup.sh'
# Docker
COMPOSE_FILE="${INFR_DEPLOY_DIR}/monitoring/docker-compose.backup-node.yml"


###############################################################################
# Main code
#------------------------------------------------------------------------------
echo "Set the password for $ADMIN_USERNAME:" && passwd "$ADMIN_USERNAME"

# Security configuration (calling library function)
echo -e '\n+ Security configuration...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Security_Configuration.lib"
Security_Configuration &> /dev/null

#------------------------------------------------------------------------------
# Installing docker
#------------------------------------------------------------------------------
echo '+ Installing docker...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Install_Docker.lib"
Install_Docker &> /dev/null

#------------------------------------------------------------------------------
# apt
#------------------------------------------------------------------------------
echo '+ Installing necessary apt packages...'
apt --yes install rsync &> /dev/null

#------------------------------------------------------------------------------
# Iptables configuration
#------------------------------------------------------------------------------
echo "+ Iptables configuration..."
# Iptables configuration according to the principle
# "everything that is not allowed should be prohibited"
iptables --policy INPUT DROP
iptables --policy FORWARD DROP
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" --out-interface "$DOCKER_NETWORK_NAME" \
  --jump DROP
# Allowing ntp port (123/udp)
iptables --append INPUT --protocol udp --dport 123 --jump ACCEPT
# Rules for apt to work correctly
iptables --append INPUT --in-interface lo --jump ACCEPT
iptables --append INPUT --match state --state ESTABLISHED --jump ACCEPT

# Allow ssh connection from administration node
iptables \
  --append INPUT \
  --source "$ADMIN_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# allow connection via 9100 port from monitoring node for node-exporter metrics
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" \
  --source "$MON_NODE_IP" \
  --protocol tcp \
  --dport 9100 \
  --jump RETURN

# Save iptables rules
iptables-save > /etc/iptables/rules.v4

#------------------------------------------------------------------------------
# Rsync setup
#------------------------------------------------------------------------------
# Сreating directories where files will be backed up
mkdir --parents "$CA_NODE_BACKUP_DIR" "$VPN_NODE_BACKUP_DIR"

echo "+ Installing rsync-systemd-setup_1.0.deb (rsync systemd setup)..."
# Set up systemd service and timer files for scheduled launch
dpkg --install "${DEB_PKG_DIR}/rsync-systemd-setup_1.0.deb" &> /dev/null

# Inserting IPs for backup into /usr/local/bin/backup.sh
sed \
  --in-place "$RSYNC_SCRIPT_FILE" \
  --expression "s/caNodeAddress2Replace/${ADMIN_USERNAME}@${CA_NODE_IP}/" \
  --expression "s/vpnNodeAddress2Replace/${ADMIN_USERNAME}@${VPN_NODE_IP}/" \
  --expression "s/alertEmail2Replace/${ALERT_EMAIL}/" \
  --expression "s/alertEmailPassword2Replace/${ALERT_EMAIL_PASSWORD}/"

systemctl daemon-reload
systemctl start rsync-backup.service
systemctl enable rsync-backup.service rsync-backup.timer &> /dev/null

#------------------------------------------------------------------------------
# node-exporter setup
#   node-exporter docker image: https://hub.docker.com/r/prom/node-exporter
#------------------------------------------------------------------------------
echo '+ Running docker compose...'
docker compose --file "$COMPOSE_FILE" up --detach &> /dev/null

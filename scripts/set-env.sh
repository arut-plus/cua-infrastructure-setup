#!/usr/bin/env bash
export ADMIN_USERNAME='admin'

export INFR_DEPLOY_DIR="/home/${ADMIN_USERNAME}/cua-infrastructure-setup"
export DEB_PKG_DIR="${INFR_DEPLOY_DIR}/deb_packages"
export LIB_DIR="${INFR_DEPLOY_DIR}/scripts/lib"

export CA_NODE_IP='10.1.2.3'
export VPN_NODE_IP='10.1.2.4'
export BCK1_NODE_IP='10.1.2.5'
export BCK2_NODE_IP='10.1.2.6'
export MON_NODE_IP='10.1.2.7'

# For Monitoring and Backup nodes
export PROMETHEUS_PASSWORD=''
export ALERT_EMAIL=''
export ALERT_EMAIL_PASSWORD=''

#!/usr/bin/env bash
# deploy-CA-node.sh: Deploys and configures the CA server (EasyRSA)
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#
# Argument support: no
#______________________________________________________________________________

###############################################################################
# Help text
#------------------------------------------------------------------------------
# About using bash idioms (source: "bash Idioms. Carl Albing & JP Vossen" book)
#   The bash idiom is used instead of the `if` operator,
#   because `if` operator for the case with 1-2 commands
#   in the then/else branch is too large and unwieldy.
#
# ./easyrsa keys explanation:
#   --sbatch  -- removes the need to confirm your intentions once again


###############################################################################
# Error Handling
#------------------------------------------------------------------------------
# A handler function for terminating signals and errors,
# which should clean up after itself
#   Usage: Error_Handler
#   Global vars:      no
#   Input parameters: no
#   Output values:    no
function Error_Handler {
    local exit_code="$?"

    echo "Error: exit code: $exit_code" >&2

    echo 'Disabling the net configuration...' >&2
    iptables --flush

    # [?] For an explanation, see line 13
    echo 'Removing created dirs...'
    [[ -d "$EASYRSA_DIR" ]] && rm --recursive --force "$EASYRSA_DIR"

    if docker ps | grep --quiet 'node-exporter'; then
        echo "Stoping all containers..."
        docker compose --file "$COMPOSE_FILE" down
    fi

    exit "$exit_code"
}  # End of Error_Handler function

# Unofficial strict mode
#   -e  --  Ends the script on the first error
#   -E  --  Allows the trap command to work
#   -u  --  When trying to access undefined var the script terminates
#   -o pipefail  -- Terminates the script if any command in the pipeline fails
set -eEuo pipefail

# A signal trap that calls the Error_Handler function
#   ABRT - abort interrupt signal
#   HUP  - terminal restart signal
#   INT  - interrupt signal (Ctrl+C)
#   QUIT - exit signal (Ctrl+C)
#   TERM - termination signal (allows the process to terminate gracefully)
#   ERR  - error signal
trap Error_Handler ABRT HUP INT QUIT TERM ERR

# [?] For an explanation, see line 13
# Outputs an error (in STDERR) and terminates the script
# if the script is run without superuser rights
(( EUID != 0 )) && { echo 'The script needs superuser rights.' >&2; exit 1; }


###############################################################################
# Global variables/constants
#------------------------------------------------------------------------------
# Sourcing environment variables from env-file
source /home/admin/cua-infrastructure-setup/scripts/set-env.sh
# Getting the IP from which you are currently connected via ssh
# (that is, administration node) for iptables rules configuration
ADMIN_NODE_IP="$(
    who --ips | grep "$ADMIN_USERNAME" | awk '{print $5}' | sort --unique
)"
# Deployment environment information
DOCKER_NETWORK_NAME="ca-docker-net"
NET_INTERFACE_NAME="$(ip route | grep 'default' | awk '{print $5}')"
# Directories
EASYRSA_DIR="/home/${ADMIN_USERNAME}/easy-rsa"
# Docker
COMPOSE_FILE="${INFR_DEPLOY_DIR}/monitoring/docker-compose.CA-node.yml"


###############################################################################
# Main code
#------------------------------------------------------------------------------
echo "Set the password for $ADMIN_USERNAME:" && passwd "$ADMIN_USERNAME"

# Security configuration (calling library function)
echo -e '\n+ Security configuration...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Security_Configuration.lib"
Security_Configuration &> /dev/null

#------------------------------------------------------------------------------
# Installing docker
#------------------------------------------------------------------------------
echo '+ Installing docker...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Install_Docker.lib"
Install_Docker &> /dev/null

#------------------------------------------------------------------------------
# Iptables configuration
#------------------------------------------------------------------------------
echo "+ Iptables configuration..."
# Iptables configuration according to the principle
# "everything that is not allowed should be prohibited"
iptables --policy INPUT DROP
iptables --policy FORWARD DROP
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" --out-interface "$DOCKER_NETWORK_NAME" \
  --jump DROP
# Allowing ntp port (123/udp)
iptables --append INPUT --protocol udp --dport 123 --jump ACCEPT
# Rules for apt to work correctly
iptables --append INPUT --in-interface lo --jump ACCEPT
iptables --append INPUT --match state --state ESTABLISHED --jump ACCEPT

# Allow ssh connection from administration node
iptables \
  --append INPUT \
  --source "$ADMIN_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# allow ssh connection
#   from VPN_NODE for signing requests
#   from BCK1_NODE_IP, BCK2_NODE_IP for rsync
iptables \
  --append INPUT \
  --source "$VPN_NODE_IP","$BCK1_NODE_IP","$BCK2_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# allow connection via 9100 port from monitoring node for node-exporter metrics
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" \
  --source "$MON_NODE_IP" \
  --protocol tcp \
  --dport 9100 \
  --jump RETURN

# Save iptables rules
iptables-save > /etc/iptables/rules.v4

#------------------------------------------------------------------------------
# apt
#------------------------------------------------------------------------------
echo '+ Installing necessary apt packages...'
{
    apt update
    apt --yes  install \
      easy-rsa \
      rsync
} &> /dev/null

#------------------------------------------------------------------------------
# easy-rsa setup
#------------------------------------------------------------------------------
mkdir --parents "$EASYRSA_DIR"

# $_ - last argument (in this case, cd to $EASYRSA_DIR)
ln --symbolic /usr/share/easy-rsa/* "$EASYRSA_DIR" && cd "$_"

echo -e "+ Initializing the PKI...\n"
# Initializing the PKI
# [?] For an explanation, see line 18
./easyrsa --sbatch init-pki

echo '+ Installing easyrsa-config_1.0.deb (easy-rsa configuration)...'
# Setting the EasyRSA configuration
dpkg --install "${DEB_PKG_DIR}/easyrsa-config_1.0.deb" &> /dev/null

# Building the CA server 
# [?] For an explanation, see line 18
./easyrsa --sbatch build-ca

# Symbols are used instead of numbers, because it is more readable
chmod a+rwx,g-rwx,o-rwx "$EASYRSA_DIR"

chown --recursive "${ADMIN_USERNAME}:${ADMIN_USERNAME}" "$EASYRSA_DIR"

#------------------------------------------------------------------------------
# node-exporter setup
#   node-exporter docker image: https://hub.docker.com/r/prom/node-exporter
#------------------------------------------------------------------------------
echo -e '\n+ Running docker compose...'
docker compose --file "$COMPOSE_FILE" up --detach &> /dev/null

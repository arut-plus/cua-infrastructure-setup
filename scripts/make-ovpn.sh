#!/usr/bin/env bash
# make-ovpn.sh:
#   Creates a client config (.ovpn), signing the certificate before that.
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#
# Argument support: yes (use [-h|--help] for usage info)
#______________________________________________________________________________

###############################################################################
# Help text
#------------------------------------------------------------------------------
# About using bash idioms (source: "bash Idioms. Carl Albing & JP Vossen" book)
#   The bash idiom is used instead of the `if` operator,
#   because `if` operator for the case with 1-2 commands
#   in the then/else branch is too large and unwieldy.
#
# ./easyrsa keys explanation:
#   --sbatch  -- removes the need to confirm your intentions once again


###############################################################################
# Help output
# Simple parsing [-h|--help] keys (w/o getopts, cuz it's unnecessary here)
#------------------------------------------------------------------------------
PROGRAM="${0##*/}"  # bash version of the `basename` command

# If there are fewer than 1 argument or the first argument is -h/--help
if [[ $# -lt 1 || "$1" == "-h" || "$1" == "--help" ]]; then
    # Indents in the lines after EoH (End-of-Help), decorated with tabs!
    # The dash (-) after << removes tabs when output (but not spaces)
	cat <<-EoH
		$PROGRAM
		Creates a client config (.ovpn), signing the certificate before that.
		(!! superuser rights required)
		----------------------------------------------------------------------

		USAGE:
		    $PROGRAM <client_id>
		    $PROGRAM [-h|--help]
		OPTIONS:
		    -h, --help    Print this help message

		-------------------------------------------------
		IN WHAT FORM EXACTLY SHOULD I PASS THE ARGUMENT?

		In <client_id> it is necessary to pass the client identifier.
		An arbitrary name to be used in the name of the configuration file.
		For example, 'client1', 'JohnDoe' or smth else
	EoH
	# Most of the indents in the line above are tabbed!
	# After 'USAGE: and 'OPTIONS:' some indents are decorated with spaces
	# to be preserved in the output.

	exit 1
fi


###############################################################################
# Error Handling
#------------------------------------------------------------------------------
# A handler function for terminating signals and errors,
# which should clean up after itself
#   Usage: Error_Handler
#   Global vars:      no
#   Input parameters: no
#   Output values:    no
function Error_Handler {
    local exit_code="$?"

    echo "Error: exit code: $exit_code" >&2

    echo "Removing ${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn..."
    # [?] For an explanation, see line 14
    # Deletes the client config if it exists
    [[ -e "${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn" ]] \
      && rm "${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn"
    # Deletes files in directiory if it's not empty
    [[ -n "$(ls --almost-all "$TMP_DIR")" ]] && rm --force "$TMP_DIR"/*

    exit "$exit_code"
}  # End of Error_Handler function


# Unofficial strict mode
#   -e  --  Ends the script on the first error
#   -E  --  Allows the trap command to work
#   -u  --  When trying to access undefined var the script terminates
#   -o pipefail  -- Terminates the script if any command in the pipeline fails
set -eEuo pipefail

# A signal trap that calls the Error_Handler function
#   ABRT - abort interrupt signal
#   HUP  - terminal restart signal
#   INT  - interrupt signal (Ctrl+C)
#   QUIT - exit signal (Ctrl+C)
#   TERM - termination signal (allows the process to terminate gracefully)
#   ERR  - error signal
trap Error_Handler ABRT HUP INT QUIT TERM ERR

# [?] For an explanation, see line 14
# Outputs an error (in STDERR) and terminates the script
# if the script is run without superuser rights
(( EUID != 0 )) && { echo 'The script needs superuser rights.' >&2; exit 1; }


###############################################################################
# Global variables/constants
#------------------------------------------------------------------------------
# Sourcing environment variables from env-file
source /home/admin/cua-infrastructure-setup/scripts/set-env.sh
# Options
CA_SSH_HOSTNAME="${ADMIN_USERNAME}@${CA_NODE_IP}"
CLIENT_ID="$1"
# Directories
EASYRSA_DIR="/home/${ADMIN_USERNAME}/easy-rsa"
TMP_DIR="/home/${ADMIN_USERNAME}/tmp"
CLIENT_FILES_DIR="/home/${ADMIN_USERNAME}/OpenVPN-clients"
CLIENT_KEYS_DIR="${CLIENT_FILES_DIR}/keys"
CLIENT_CONFIGS_DIR="${CLIENT_FILES_DIR}/configs"
# Files
CLIENT_CONFIG_TEMPLATE="${CLIENT_FILES_DIR}/client.conf"


###############################################################################
# Main code
#------------------------------------------------------------------------------
cd "$EASYRSA_DIR"
# Generating a client certificate request (without passphrase)
# [?] For an explanation, see line 19
./easyrsa --sbatch gen-req "$CLIENT_ID" nopass

# Signing the certificate using the library function Sign_Request,
# which, in addition to $CA_SERVER_HOSTNAME,
# needs to be passed the type of request and the name of the request.
# shellcheck disable=SC1091
source "${LIB_DIR}/Sign_Request.lib"
Sign_Request "$CA_SSH_HOSTNAME" client "$CLIENT_ID"

# Copying the secret client key and client certificate
# to the directory with client keys.
cp \
  "${EASYRSA_DIR}/pki/private/${CLIENT_ID}.key" \
  "${TMP_DIR}/${CLIENT_ID}.crt" \
  "${CLIENT_FILES_DIR}/keys/"

chown --recursive \
  "${ADMIN_USERNAME}:${ADMIN_USERNAME}" \
  "${CLIENT_FILES_DIR}/keys"

# Concatenation of
# the client config template,
# the server CA root certificate,
# the client certificate,
# the client secret key
# and the additional secret key for tls-crypt
# into a ready-made client config (.ovpn).
cat "$CLIENT_CONFIG_TEMPLATE" \
  <(echo -e '<ca>') \
  "${CLIENT_KEYS_DIR}/ca.crt" \
  <(echo -e '</ca>\n<cert>') \
  "${CLIENT_KEYS_DIR}/${CLIENT_ID}.crt" \
  <(echo -e '</cert>\n<key>') \
  "${CLIENT_KEYS_DIR}/${CLIENT_ID}.key" \
  <(echo -e '</key>\n<tls-crypt>') \
  "${CLIENT_KEYS_DIR}/ta.key" \
  <(echo -e '</tls-crypt>') \
  > "${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn"

chown --recursive \
  "${ADMIN_USERNAME}:${ADMIN_USERNAME}" \
  "${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn"

chown --recursive "$ADMIN_USERNAME":"$ADMIN_USERNAME" "$EASYRSA_DIR"

# Shows the location of the client configuration
echo -e "\n${CLIENT_ID}.ovpn location: ${CLIENT_CONFIGS_DIR}/${CLIENT_ID}.ovpn"
# Cleanup
rm --force "$TMP_DIR"/*

#!/usr/bin/env bash
# deploy-VPN-node.sh:
#   Deploys and configures the OpenVPN server (also works with the CA server)
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#
#______________________________________________________________________________

###############################################################################
# Help text
#------------------------------------------------------------------------------
# About using bash idioms (source: "bash Idioms. Carl Albing & JP Vossen" book)
#   The bash idiom is used instead of the `if` operator,
#   because `if` operator for the case with 1-2 commands
#   in the then/else branch is too large and unwieldy.
#
# ./easyrsa keys explanation:
#   --sbatch  -- removes the need to confirm your intentions once again

###############################################################################
# Error Handling
#------------------------------------------------------------------------------
# A handler function for terminating signals and errors,
# which should clean up after itself
#   Usage: Error_Handler
#   Global vars:      no
#   Input parameters: no
#   Output values:    no
function Error_Handler {
    local exit_code="$?"
    local dir_to_remove_list=(
      "$CLIENT_FILES_DIR"
      "$EASYRSA_DIR"
      "$TMP_DIR"
    )

    echo "Error: exit code: $exit_code" >&2

    echo 'Disabling the net configuration...' >&2
    iptables --flush

    echo "Removing created dirs..."
    for dir in "${dir_to_remove_list[@]}"; do
        # [?] For an explanation, see line 14
        # Deletes the directory if it exists
        [[ -d "$dir" ]] && rm --recursive --force "$dir"
    done

    local containers_string='openvpn-exporter|node-exporter'
    if docker ps | grep --extended-regexp --quiet "$containers_string"; then
        echo "Stoping all containers..."
        docker compose --file "$COMPOSE_FILE" down
    fi

    exit "$exit_code"
}  # End of Error_Handler function


# Unofficial strict mode
#   -e  --  Ends the script on the first error
#   -E  --  Allows the trap command to work
#   -u  --  When trying to access undefined var the script terminates
#   -o pipefail  -- Terminates the script if any command in the pipeline fails
set -eEuo pipefail

# A signal trap that calls the Error_Handler function
#   ABRT - abort interrupt signal
#   HUP  - terminal restart signal
#   INT  - interrupt signal (Ctrl+C)
#   QUIT - exit signal (Ctrl+C)
#   TERM - termination signal (allows the process to terminate gracefully)
#   ERR  - error signal
trap Error_Handler ABRT HUP INT QUIT TERM ERR

# [?] For an explanation, see line 14
# Outputs an error (in STDERR) and terminates the script
# if the script is run without superuser rights
(( EUID != 0 )) && { echo 'The script needs superuser rights.' >&2; exit 1; }


###############################################################################
# Global/env variables/constants
#------------------------------------------------------------------------------
# Sourcing environment variables from env-file
source /home/admin/cua-infrastructure-setup/scripts/set-env.sh
# Getting the IP from which you are currently connected via ssh
# (that is, administration node) for iptables rules configuration
ADMIN_NODE_IP="$(
    who --ips | grep "$ADMIN_USERNAME" | awk '{print $5}' | sort --unique
)"
# Deployment environment information
DOCKER_NETWORK_NAME="vpn-docker-net"
NET_INTERFACE_NAME="$(ip route | grep 'default' | awk '{print $5}')"
VPN_SERVER_PUBLIC_IP="$(dig +short myip.opendns.com @resolver1.opendns.com)"
# Options
CA_NODE_SSH_HOSTNAME="${ADMIN_USERNAME}@${CA_NODE_IP}"
# Directories that this script creates
EASYRSA_DIR="/home/${ADMIN_USERNAME}/easy-rsa"
CLIENT_FILES_DIR="/home/${ADMIN_USERNAME}/OpenVPN-clients"
TMP_DIR="/home/${ADMIN_USERNAME}/tmp"
# Docker
COMPOSE_FILE="${INFR_DEPLOY_DIR}/monitoring/docker-compose.VPN-node.yml"


###############################################################################
# Main code
#------------------------------------------------------------------------------
echo "Set the password for $ADMIN_USERNAME:" && passwd "$ADMIN_USERNAME"

# Security configuration (calling library function)
echo -e '\n+ Security configuration...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Security_Configuration.lib"
Security_Configuration &> /dev/null

#------------------------------------------------------------------------------
# Installing docker
#------------------------------------------------------------------------------
echo '+ Installing docker...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Install_Docker.lib"
Install_Docker &> /dev/null

#------------------------------------------------------------------------------
# apt
#------------------------------------------------------------------------------
echo '+ Installing necessary apt packages...'
{
    apt update
    apt --yes install \
      easy-rsa \
      openvpn \
      rsync
} &> /dev/null

#------------------------------------------------------------------------------
# Iptables configuration
#------------------------------------------------------------------------------
echo "+ Iptables configuration..."
# Iptables configuration according to the principle
# "everything that is not allowed should be prohibited"
iptables --policy INPUT DROP
iptables --policy FORWARD DROP
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" --out-interface "$DOCKER_NETWORK_NAME" \
  --jump DROP
# Allowing ntp port (123/udp)
iptables --append INPUT --protocol udp --dport 123 --jump ACCEPT
# Rules for apt to work correctly
iptables --append INPUT --in-interface lo --jump ACCEPT
iptables --append INPUT --match state --state ESTABLISHED --jump ACCEPT

# Allow ssh connection from administration node
iptables \
  --append INPUT \
  --source "$ADMIN_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# Allow ssh connection from BCK1_NODE_IP, BCK2_NODE_IP for rsync
iptables \
  --append INPUT \
  --source "$BCK1_NODE_IP","$BCK2_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# Allowing connections from monitoring node to collect metrics
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" \
  --source "$MON_NODE_IP" \
  --protocol tcp \
  --match multiport --dport 9100,9176 \
  --jump RETURN

# Allowing UDP port 1194 for OpenVPN
iptables \
  --append INPUT \
  --in-interface "$NET_INTERFACE_NAME" \
  --match state --state NEW \
  --protocol udp --dport 1194 \
  --jump ACCEPT
# Allow TUN interface connections to OpenVPN server
iptables --append INPUT --in-interface tun+ --jump ACCEPT
# Allow TUN interface connections to be forwarded through other interfaces
iptables --append FORWARD --in-interface tun+ --jump ACCEPT
iptables \
  --append FORWARD \
  --in-interface tun+ --out-interface "$NET_INTERFACE_NAME" \
  --match state --state RELATED,ESTABLISHED \
  --jump ACCEPT
iptables \
  --append FORWARD \
  --in-interface "$NET_INTERFACE_NAME" --out-interface tun+ \
  --match state --state RELATED,ESTABLISHED \
  --jump ACCEPT
# NAT the VPN client traffic to the Internet
iptables \
  --table nat \
  --append POSTROUTING \
  --source 10.8.0.0/24 \
  --out-interface "$NET_INTERFACE_NAME" \
  --jump MASQUERADE
# Allow outgoing traffic through the tun+ interface
iptables --append OUTPUT --out-interface tun+ --jump ACCEPT

# Save iptables rules
iptables-save > /etc/iptables/rules.v4

#------------------------------------------------------------------------------
# easy-rsa setup & managing certificates, keys, etc.
#------------------------------------------------------------------------------
mkdir --parents \
  "$EASYRSA_DIR" \
  "$TMP_DIR" \
  "$CLIENT_FILES_DIR"/{keys,configs}
  # Final hierarchy:
  # - $EASYRSA_DIR/
  # - $TMP_DIR/
  # - $CLIENT_FILES_DIR/ - keys/
  #                      ` configs/

# $_ - last argument (in this case, cd to $EASYRSA_DIR)
ln --symbolic /usr/share/easy-rsa/* "$EASYRSA_DIR" && cd "$_"

echo -e "+ Initializing the PKI and signing request...\n"
# Initializing the PKI
# [?] For an explanation, see line 19
./easyrsa --sbatch init-pki &> /dev/null

# Generating a server certificate request (without passphrase)
# [?] For an explanation, see line 19
./easyrsa --sbatch gen-req server nopass &> /dev/null

# Signing the certificate using the library function Sign_Request,
# which, in addition to $CA_SERVER_HOSTNAME,
# needs to be passed the type of request and the name of the request.
# shellcheck disable=SC1091
source "${LIB_DIR}/Sign_Request.lib"
Sign_Request "$CA_NODE_SSH_HOSTNAME" server server

# Generating a secret key for tls-crypt
openvpn --genkey secret "${TMP_DIR}/ta.key" &> /dev/null

# -P  -- specify port on remote host
# -i  -- select identity file
scp \
  -P 2222 \
  -i "/home/${ADMIN_USERNAME}/.ssh/id_rsa" \
  "${CA_NODE_SSH_HOSTNAME}:${EASYRSA_DIR}/pki/ca.crt" \
  "$TMP_DIR" &> /dev/null

# Symbols are used instead of numbers, because it's more readable
chmod a+rwx,g-rwx,o-rwx "$TMP_DIR" "$EASYRSA_DIR" "$CLIENT_FILES_DIR"

cp \
  "${EASYRSA_DIR}/pki/private/server.key" \
  "$TMP_DIR"/{ca.crt,server.crt,ta.key} \
  /etc/openvpn/server/

cp \
  "$TMP_DIR"/{ta.key,ca.crt} \
  "${CLIENT_FILES_DIR}/keys/"

# Cleanup
rm --force "$TMP_DIR"/*

chown --recursive "$ADMIN_USERNAME":"$ADMIN_USERNAME" \
  "$EASYRSA_DIR" "$CLIENT_FILES_DIR"

#------------------------------------------------------------------------------
# OpenVPN configuration
#------------------------------------------------------------------------------
echo -e '\n+ Installing openvpn-config_1.0.deb (OpenVPN configuration)...'
# Setting OpenVPN configuration
dpkg --install "${DEB_PKG_DIR}/openvpn-config_1.0.deb" &> /dev/null
# Setting VPN server public IP in VPN server configuration
sed \
  --in-place "s/remote_server_ip/${VPN_SERVER_PUBLIC_IP}/" \
  "${CLIENT_FILES_DIR}/client.conf"

# Enables IP forwarding for VPN server operation
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl --load --quiet

# Enabling autorun and restarting the OpenVPN server to apply changes
systemctl --force enable openvpn-server@server.service &> /dev/null
systemctl restart openvpn-server@server.service

#------------------------------------------------------------------------------
# OpenVPN Exporter + node-exporter setup
#   OpenVPN exporter docker image and source code:
#       https://github.com/theohbrothers/openvpn_exporter
#       https://hub.docker.com/r/theohbrothers/openvpn_exporter
#   node-exporter docker image: https://hub.docker.com/r/prom/node-exporter
#------------------------------------------------------------------------------
echo '+ Running docker compose...'
docker compose --file "$COMPOSE_FILE" up --detach &> /dev/null

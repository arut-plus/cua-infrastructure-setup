#!/usr/bin/env bash
# deploy-monitoring-node.sh:
#   Deploys Prometheus, Grafana, Alertmanager
#   to monitor all nodes in the infrastructure
# Author: Arut Meliqsetyan (arut_plus)
# Written for the final project of the course
# "Start in DevOps: system administration for beginners"
#______________________________________________________________________________

###############################################################################
# Error Handling
#------------------------------------------------------------------------------
# A handler function for terminating signals and errors,
# which should clean up after itself
#   Usage: Error_Handler
#   Global vars:      no
#   Input parameters: no
#   Output values:    no
function Error_Handler {
    local exit_code="$?"

    echo "Error: exit code: $exit_code" >&2

    echo 'Disabling the net configuration...' >&2
    iptables --flush

    local containers_string='grafana|alertmanager|prometheus|node-exporter'
    if docker ps | grep --extended-regexp --quiet "$containers_string"; then
        echo "Stoping all containers..."
        docker compose --file "$COMPOSE_FILE" down
    fi

    exit "$exit_code"
}  # End of Error_Handler function


# Unofficial strict mode
#   -e  --  Ends the script on the first error
#   -E  --  Allows the trap command to work
#   -u  --  When trying to access undefined var the script terminates
#   -o pipefail  -- Terminates the script if any command in the pipeline fails
set -eEuo pipefail

# A signal trap that calls the Error_Handler function
#   ABRT - abort interrupt signal
#   HUP  - terminal restart signal
#   INT  - interrupt signal (Ctrl+C)
#   QUIT - exit signal (Ctrl+C)
#   TERM - termination signal (allows the process to terminate gracefully)
#   ERR  - error signal
trap Error_Handler ABRT HUP INT QUIT TERM ERR

# Outputs an error (in STDERR) and terminates the script
# if the script is run without superuser rights
#   About using bash idioms
#       (source: "bash Idioms. Carl Albing & JP Vossen" book)
#       The bash idiom is used instead of the `if` operator,
#       because `if` operator for the case with 1-2 commands
#       in the then/else branch is too large and unwieldy.
(( EUID != 0 )) && { echo 'The script needs superuser rights.' >&2; exit 1; }


###############################################################################
# Global variables/constants
#------------------------------------------------------------------------------
# Sourcing environment variables from env-file
source /home/admin/cua-infrastructure-setup/scripts/set-env.sh
# Getting the IP from which you are currently connected via ssh
# (that is, administration node) for iptables rules configuration
ADMIN_NODE_IP="$(
    who --ips | grep "$ADMIN_USERNAME" | awk '{print $5}' | sort --unique
)"
# Deployment environment information
DOCKER_NETWORK_NAME="mon-docker-net"
NET_INTERFACE_NAME="$(ip route | grep 'default' | awk '{print $5}')"
# Directories
MONITORING_DIR="${INFR_DEPLOY_DIR}/monitoring"
# Files
PROMETHEUS_CONFIG_FILE="${MONITORING_DIR}/prometheus/prometheus.yml"
PROMETHEUS_WEB_CONFIG_FILE="${MONITORING_DIR}/prometheus/web.yml"
ALERTMANAGER_CONFIG_FILE="${MONITORING_DIR}/alertmanager/alertmanager.yml"
# Docker
COMPOSE_FILE="${MONITORING_DIR}/docker-compose.monitoring-node.yml"


###############################################################################
# Main code
#------------------------------------------------------------------------------
echo "Set the password for $ADMIN_USERNAME:" && passwd "$ADMIN_USERNAME"

# Security configuration (calling library function)
echo -e '\n+ Security configuration...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Security_Configuration.lib"
Security_Configuration &> /dev/null

#------------------------------------------------------------------------------
# Installing docker
#------------------------------------------------------------------------------
echo '+ Installing docker...'
# shellcheck disable=SC1091
source "${LIB_DIR}/Install_Docker.lib"
Install_Docker &> /dev/null

#------------------------------------------------------------------------------
# Apt
#------------------------------------------------------------------------------
echo '+ Installing necessary apt packages...'
# for htpasswd to generate password hashes
apt --yes install apache2-utils &> /dev/null

#------------------------------------------------------------------------------
# Prometheus and alertmanager configuration files
#------------------------------------------------------------------------------
# Generating password hashes for prometheus web.yml
#   htpasswd keys:
#       -i  Read password from stdin w/o verification
#       -n  Don't update file; display results on stdout
#       -B  Force bcrypt encryption of the password (very secure)
#       -C  Set the computing time used for the bcrypt algorithm
PROMETHEUS_PASSWORD_HASH="$(
    htpasswd -inBC 10 "" <<< "$PROMETHEUS_PASSWORD" \
      | tr --delete  ':\n'
)"
# Substituting password hash in prometheus web.yml
echo -e "basic_auth_users:\n  admin: $PROMETHEUS_PASSWORD_HASH" \
  >> "$PROMETHEUS_WEB_CONFIG_FILE"

# cuz prometheus doesn't support fully env variables in configuration files
# https://github.com/prometheus/alertmanager/issues/504
sed \
  --in-place "$ALERTMANAGER_CONFIG_FILE" \
  --expression "s/alertEmail2Replace/${ALERT_EMAIL}/" \
  --expression "s/alertEmailPassword2Replace/${ALERT_EMAIL_PASSWORD}/"
sed \
  --in-place "$PROMETHEUS_CONFIG_FILE" \
  --expression "s/caNodeAddress2Replace/${CA_NODE_IP}/g" \
  --expression "s/vpnNodeAddress2Replace/${VPN_NODE_IP}/g" \
  --expression "s/bck1NodeAddress2Replace/${BCK1_NODE_IP}/g" \
  --expression "s/bck2NodeAddress2Replace/${BCK2_NODE_IP}/g" \
  --expression "s/prometheusPassword2Replace/${PROMETHEUS_PASSWORD}/"

#------------------------------------------------------------------------------
# Iptables configuration
#------------------------------------------------------------------------------
echo "+ Iptables configuration..."
# Iptables configuration according to the principle
# "everything that is not allowed should be prohibited"
iptables --policy INPUT DROP
iptables --policy FORWARD DROP

# Allow all traffic from the Docker network interface
iptables \
  --insert DOCKER-USER \
  --in-interface "$DOCKER_NETWORK_NAME" \
  --jump RETURN

# Allowing ntp port (123/udp)
iptables --append INPUT --protocol udp --dport 123 --jump ACCEPT

# Rules for apt to work correctly
iptables --append INPUT --in-interface lo --jump ACCEPT
iptables --append INPUT --match state --state ESTABLISHED --jump ACCEPT

# Allow ssh connection from administration node
iptables \
  --append INPUT \
  --source "$ADMIN_NODE_IP" \
  --protocol tcp \
  --dport 2222 \
  --jump ACCEPT

# Allow ports 3000, 9090 to all (grafana and prometheus)
iptables \
  --insert DOCKER-USER \
  --in-interface "$NET_INTERFACE_NAME" \
  --protocol tcp \
  --match multiport --dport 3000,9090 \
  --jump RETURN

# Allow ports 9100,9093 only for localhost (node-exporter and alertmanager)
iptables \
  --insert DOCKER-USER \
  --in-interface lo \
  --protocol tcp \
  --match multiport --dport 9100,9093 \
  --jump RETURN

# Save iptables rules
iptables-save > /etc/iptables/rules.v4

#------------------------------------------------------------------------------
# Grafana + Prometheus + Alertmanager + node-exporter setup
#   Grafana docker image: https://hub.docker.com/r/grafana/grafana
#   Prometheus docker image: https://hub.docker.com/r/prom/prometheus
#   Alertmanager docker image: https://hub.docker.com/r/prom/alertmanager
#   node-exporter docker image: https://hub.docker.com/r/prom/node-exporter
#------------------------------------------------------------------------------
echo '+ Running docker compose...'
docker compose --file "$COMPOSE_FILE" up --detach &> /dev/null

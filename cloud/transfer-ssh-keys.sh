#!/usr/bin/env bash

function Get_Node_Public_IP {
	local node_name="$1"
	yc compute instance get "$node_name" --format json \
	  | jq --raw-output '.network_interfaces[0].primary_v4_address.one_to_one_nat.address'
}

CA_SSH_HOSTNAME="admin@$(Get_Node_Public_IP ca-node)"
VPN_SSH_HOSTNAME="admin@$(Get_Node_Public_IP vpn-node)"
BCK1_SSH_HOSTNAME="admin@$(Get_Node_Public_IP backup1-node)"
BCK2_SSH_HOSTNAME="admin@$(Get_Node_Public_IP backup2-node)"

for src in "$VPN_SSH_HOSTNAME" "$BCK1_SSH_HOSTNAME" "$BCK2_SSH_HOSTNAME"; do
	for dest in "$CA_SSH_HOSTNAME" "$VPN_SSH_HOSTNAME"; do
		if [[ "$src" != "$dest" ]]; then
			ssh "$src" 'cat ~/.ssh/id_rsa.pub' \
			  | ssh "$dest" 'cat >> ~/.ssh/authorized_keys'
		fi
	done
done

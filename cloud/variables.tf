variable "CA_NODE_IP" {
  description = "IP address for ca-node"
  type        = string
}
variable "VPN_NODE_IP" {
  description = "IP address for vpn-node"
  type        = string
}
variable "BCK1_NODE_IP" {
  description = "IP address for backup1-node"
  type        = string
}
variable "BCK2_NODE_IP" {
  description = "IP address for backup2-node"
  type        = string
}
variable "MON_NODE_IP" {
  description = "IP address for monitoring-node"
  type        = string
}

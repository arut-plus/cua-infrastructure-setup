terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file("authorized_key.json")
  cloud_id                 = "b1g1oeck1btfesbnmpi3"
  folder_id                = "b1gd65iris0u7l08oj5o"
  zone                     = "ru-central1-a"
}

resource "yandex_compute_disk" "boot-disk-vm-1" {
  name     = "boot-disk-vm-1"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = 5
  image_id = "fd801rku4j14mv7fs703"
}
resource "yandex_compute_disk" "boot-disk-vm-2" {
  name     = "boot-disk-vm-2"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = 5
  image_id = "fd801rku4j14mv7fs703"
}
resource "yandex_compute_disk" "boot-disk-vm-3" {
  name     = "boot-disk-vm-3"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = 5
  image_id = "fd801rku4j14mv7fs703"
}
resource "yandex_compute_disk" "boot-disk-vm-4" {
  name     = "boot-disk-vm-4"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = 5
  image_id = "fd801rku4j14mv7fs703"
}
resource "yandex_compute_disk" "boot-disk-vm-5" {
  name     = "boot-disk-vm-5"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = 5
  image_id = "fd801rku4j14mv7fs703"
}

resource "yandex_compute_instance" "vm-1" {
  name        = "ca-node"
  hostname    = "ca-node"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-vm-1.id
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.cua-yc-subnet-a.id
    nat        = true
    ip_address = var.CA_NODE_IP
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }
}

resource "yandex_compute_instance" "vm-2" {
  name        = "vpn-node"
  hostname    = "vpn-node"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-vm-2.id
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.cua-yc-subnet-a.id
    nat        = true
    ip_address = var.VPN_NODE_IP
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }
}

resource "yandex_compute_instance" "vm-3" {
  name        = "backup1-node"
  hostname    = "backup1-node"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-vm-3.id
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.cua-yc-subnet-a.id
    nat        = true
    ip_address = var.BCK1_NODE_IP
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }
}

resource "yandex_compute_instance" "vm-4" {
  name        = "backup2-node"
  hostname    = "backup2-node"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-vm-4.id
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.cua-yc-subnet-a.id
    nat        = true
    ip_address = var.BCK2_NODE_IP
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }
}

resource "yandex_compute_instance" "vm-5" {
  name        = "monitoring-node"
  hostname    = "monitoring-node"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-vm-5.id
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.cua-yc-subnet-a.id
    nat        = true
    ip_address = var.MON_NODE_IP
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }
}

resource "yandex_vpc_network" "cua-yc-network" {
  name = "cua-yc-network"
}

resource "yandex_vpc_subnet" "cua-yc-subnet-a" {
  name           = "cua-yc-subnet-a"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.cua-yc-network.id
  v4_cidr_blocks = ["10.1.2.0/24"]
}


# resource "null_resource" "setup_ssh" {
#   depends_on = [
#     yandex_compute_instance.vm-1, yandex_compute_instance.vm-2,
#     yandex_compute_instance.vm-3, yandex_compute_instance.vm-4
#   ]
# }

output "internal_instance_ips" {
  value = {
    "vm-1" = yandex_compute_instance.vm-1.network_interface.0.ip_address
    "vm-2" = yandex_compute_instance.vm-2.network_interface.0.ip_address
    "vm-3" = yandex_compute_instance.vm-3.network_interface.0.ip_address
    "vm-4" = yandex_compute_instance.vm-4.network_interface.0.ip_address
    "vm-5" = yandex_compute_instance.vm-5.network_interface.0.ip_address
  }
}
output "external_instance_ips" {
  value = {
    "vm-1" = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
    "vm-2" = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
    "vm-3" = yandex_compute_instance.vm-3.network_interface.0.nat_ip_address
    "vm-4" = yandex_compute_instance.vm-4.network_interface.0.nat_ip_address
    "vm-5" = yandex_compute_instance.vm-5.network_interface.0.nat_ip_address
  }
}

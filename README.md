
# Инфраструктура CUA

**Инфраструктура CUA** (**C**entralized **U**ser **A**dministration) представляет собой комплексную систему, призванную/ориентированную на **централизованное управление учетными записями сотрудников и ресурсами компании** (контролируя доступ к файлам, приложениям и другим данным).

Этот репозиторий нужен для **развертки** инфраструктуры CUA. \
Проект является **финальной работой** курса "**Старт в DevOps: системное администрирование для начинающих**". \
Автор проекта: **Арут Меликсетян**

# Навигация по репозиторию

### 📸 Скриншоты

> Находятся в директории [screenshots](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/screenshots)

В директории содержатся скриншоты со следующим содержимым:
+ [Подключение к VPN. IP адрес до и после подключения на android](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/screenshots/before-and-after-VPN-connect-android.jpg)
+ [Подключение к VPN. IP адрес до и после подключения на linux](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/screenshots/before-and-after-VPN-connect-linux.png)
+ [Веб интерфейс Prometheus. Все созданные алерты](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/screenshots/prometheus-WebUI-alerts.png)
+ [Веб интерфейс Prometheus. Все источники/цели (targets)](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/screenshots/prometheus-WebUI-targets.png)
+ [Веб интерфейс Grafana. Список дашбордов, один из node дашбордов и openvpn дашборд](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/screenshots/grafana-dashboards.jpg) 


### 📚 Документация

> Вся документация находится в директории [docs](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs).
##### Гайды/инструкции
> Находятся в директории [guides](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/guides)

[Инструкция для пользователей по установке и настройке OpenVPN](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/guides/CUA-admin-guide.pdf) \
[Руководство системного администратора](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/guides/CUA-admin-guide.pdf)

В директории [guides](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/guides) есть директория [md](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/guides/md), где содержаться гайды в формате **markdown** и **медиа файлы** в директории [md/media](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/guides/md), на которые ссылаются **markdown файлы**.
##### Схемы
> Находятся в директории [schemes](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/schemes)

[Схема инфраструктуры CUA](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/schemes/CUA-infrastructure-scheme.png) \
[Схема потоков данных инфраструктуры CUA](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/schemes/CUA-infrastructure-data-flow-scheme.png)

В директории [schemes](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/schemes) есть директории [svg](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/schemes/svg) и [graphml](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/schemes/svg), где содержатся схемы в соответствующих **форматах**.


### 📝 Скрипты

> Находятся в директории [scripts](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/scripts)

Также есть директория [scripts/lib](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/scripts/lib), в котором хранятся **библиотечные скрипты**.


### 📦 DEB пакеты

> Находятся в директории [deb_packages](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/deb_packages)

Содержимое deb пакетов хранится в директории [deb_package_contents](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/deb_packages/deb_package_contents)


### 📊 Файлы для Monitoring ноды

> Находятся в директории [monitoring](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/monitoring)

В директории находятся **docker compose файлы** для каждой ноды и конфигурационные файлы для **prometheus**, **alertmanager**, **grafana**. 


### ☁️ Файлы для Terraform

> Находятся в директории [cloud](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/cloud)

В директории находятся файлы для работы **terraform** и скрипт `transfer-ssh-keys.sh` для передачи ssh ключей на нужные ВМ. \
В [Руководстве системного администратора](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/guides/CUA-admin-guide.pdf) всё это описано подробнее.

### 🚀 Планы

> Находятся в директории [docs/plans](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/tree/main/docs/plans)

[Таблица "Планы развития инфраструктуры"](https://gitlab.com/arut-plus/cua-infrastructure-setup/-/blob/main/docs/plans/Development-plans.pdf) \
В этой же директории есть эта таблица в формате **markdown**.


# Помощь

Если возникли какие-то **вопросы** или **проблемы** - обращайтесь к **администратору/создателю проекта** по **электронной почте**: test@gmail.com

##### Почему здесь практически нет коммитов?
Репозиторий проекта чистый, ибо старый пришлось удалить. Из-за разных экспериментов при работе репозиторий вырос в размере; и по ошибке было много коммитов с несодержательными сообщениями. А проект сдать хочется с чистым репозиторием.

# Инструкция для пользователей по установке и настройке OpenVPN


## Зачем использовать VPN?

> **VPN** (Виртуальная частная сеть) - это, проще говоря, **защитный туннель** для интернет-соединения.

**VPN** поможет создать **безопасное** соединение между **удаленными** сотрудниками и **внутренней сетью** компании, что обеспечит **безопасный** доступ к корпоративным ресурсам и данным, необходимым для работы, **вне** зависимости от того, **где** находится сотрудник. Это всё полезно для компании и тех, кто администрирует ваши учетные записи.

Но есть преимущества с точки зрения **простого пользователя**:

1. **Безопасность**
	Вы сможете спокойно работать через **публичные WiFi сети** (в кафе или аэропорту, например), ибо VPN **защитит** ваши данные (**шифруя** ваш интернет-трафик) от **злоумышленников**.

2. **Конфиденциальность**
	Ваш интернет-трафик станет **конфиденциальным**, то есть провайдеры/рекламные агенства/злоумышленники не смогут отслеживать вашу **активность в интернете**.

3. **Стабильный интернет**
	VPN **стабилизирует соедение и улучшит качество связи**, особенно если вы находитесь местах с нестабильным интернетом.

4. **Доступ к работе из любой точки**
	 Вы сможете использовать ресурсы и файлы вашей компании, как если бы были в офисе.

5. **Простота использования**
	**VPN**, правда, **прост** в использовании: после его настройки, чаще всего, **одним кликом** можно оказаться в **безопасной** сети и начать работать.


---

## Установка и настройка "OpenVPN клиента" на ваши устройства

> **OpenVPN клиент** - в этом случае, **приложение** OpenVPN, работающее на стороне **клиента**. Проще говоря, подключаться к VPN вы будете именно через это приложение.

Ниже будут инструкции для установки и настройки **для всех видов устройств**.

#### Получение конфигурационного файла

> Обратитесь к **администратору** для получения **конфигурационного файла OpenVPN**.
> С помощью него вы и настроите свой OpenVPN клиент. 

#### Установка OpenVPN клиента

##### iOS

1. **Установите** приложение [OpenVPN Connect](https://apps.apple.com/ru/app/openvpn-connect-openvpn-app/id590379981) из **App Store**.
2. Перейдите в приложение "**Файлы**" и найдите **файл** с расширением **.ovpn**, который вы **получили** от администратора.
3. **Поделитесь** файлом (нажмите кнопку "Поделиться"), выбрав приложение **OpenVPN Connect**:
	
	![400](media/ios-share-ovpn-to-openvpn.webp)
	
4. Откроется приложение **OpenVPN Connect**, в котором появится доступный **профиль** для импорта -  **импортируйте** профиль, нажав "**ADD**":
	
	![400](ios-import-profile-file.webp)
	
5.  **Включите** VPN:
	
	![400](ios-toggle-vpn.webp)


> Ссылки на другие гайды:
> [Гайд с официального сайта OpenVPN](https://openvpn.net/connect-docs/ios-installation-guide.html#installation-guide-for-openvpn-connect-with-openvpn-servers-58185_body)
> [Неофициальный гайд](https://thesafety.us/ru/vpn-setup-openvpn-ios15-iphone)

---
###### Android

1. **Установите** приложение [OpenVPN Connect](https://play.google.com/store/apps/details?id=net.openvpn.openvpn) из **Google Play**.
2. Перейдите в установленное приложение и выберите вкладку "**Upload File**" и нажмите ниже кнопку "**BROWSE**":
	
	 ![300](media/android-openvpn-app.png)
	
3. В открывшемся файловом менеджере найдите **файл** с расширением **.ovpn**, который вы **получили** от администратора. Согласитесь с **импортом** файла, нажав "**OK**".
4. **Включите** VPN:
	
	![400](media/android-toggle-vpn.png)


> Ссылки на другие гайды:
> [Гайд с официального сайта OpenVPN](https://openvpn.net/connect-docs/android-installation-guide.html#installation-guide-for-openvpn-connect-with-openvpn-servers-56885_body)
> [Неофициальный гайд №1](https://support.surfshark.com/hc/en-us/articles/360006143714-How-to-set-up-OpenVPN-client-on-Android)
> [Неофициальный гайд №2](https://www.ovpn.com/en/guides/android-connect)

---
###### MacOS

1. Перейдите по [этой ссылке](https://openvpn.net/client-connect-vpn-for-mac-os/) (официальный сайт OpenVPN), нажмите "**Download OpenVPN Connect v3**", чтобы установщик скачался.
2. После скачивания установщика **откройте** его и, пройдя по всем шагам, **установите** клиентское приложение:
	
	![500](media/macos-openvpn-installer.png)
	
3. Откройте установленное приложение **OpenVPN Connect**, перетащите в него **файл** с расширением **.ovpn**, который вы **получили** от администратора и импортируйте его.
	
	![300](media/macos-windows-import-file.png)
	
4. **Включите** VPN:
	
	![400](media/macos-windows-toggle-vpn.png)


> Ссылки на другие гайды:
> [Гайд с официального сайта OpenVPN](https://openvpn.net/connect-docs/macos-installation-guide.html#macos-installation-for-openvpn-connect-with-openvpn-servers_body)
> [Гайд по установке клиентского приложения с официального сайта OpenVPN](https://openvpn.net/vpn-server-resources/installation-guide-for-openvpn-connect-client-on-macos/)
> [Неофициальный гайд](https://think.unblog.ch/en/how-to-install-openvpn-client/)

---
###### Windows

1. **Скачайте** установщик с официального сайта по [этой ссылке](https://openvpn.net/client/client-connect-vpn-for-windows/).
2. Откройте скачанный **установщик** и, следуя всем шагам, **установите** его: 
	
	![400](media/windows-openvpn-installer.png)
	
3. Откройте установленное приложение **OpenVPN Connect**, перетащите в него **файл** с расширением **.ovpn**, который вы **получили** от администратора и импортируйте его.
	
	![300](media/macos-windows-import-file.png)
	
4. **Включите** VPN:
	
	![400](media/macos-windows-toggle-vpn.png)


> Ссылки на другие гайды:
> [Гайд с официального сайта OpenVPN](https://openvpn.net/connect-docs/installation-guide-windows.html)
> [Гайд по установке клиентского приложения с официального сайта OpenVPN](https://openvpn.net/vpn-server-resources/installation-guide-for-openvpn-connect-client-on-windows/)
> [Неофициальный гайд](https://think.unblog.ch/en/how-to-install-openvpn-client/)

---
###### Linux

> Инструкция по установке клиента ниже подходит для **debian-based** дистрибутивов. Если у вас другие дистрибутивы, перейдите по [первой](https://openvpn.net/cloud-docs/owner/connectors/connector-user-guides/openvpn-3-client-for-linux.html#installation-for-fedora--red-hat-enterprise-linux--centos--or-scientific-linux_body) и [второй ссылке](https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux) - следуйте инструкциям оттуда. 


1. Откройте **терминал** и установите **ключ репозитория OpenVPN** (для установки клиента из этого репозитория) и сам репозиторий в несколько команд:

```bash
sudo mkdir -p /etc/apt/keyrings && curl -fsSL https://packages.openvpn.net/packages-repo.gpg | sudo tee /etc/apt/keyrings/openvpn.asc

DISTRO=$(lsb_release -c | awk '{print $2}')

echo "deb [signed-by=/etc/apt/keyrings/openvpn.asc] https://packages.openvpn.net/openvpn3/debian $DISTRO main" | sudo tee /etc/apt/sources.list.d/openvpn-packages.list
```

2. Установите **OpenVPN 3**:

```bash
sudo apt update && sudo apt install openvpn3 -y
```

3. Импортируйте **файл** с расширением **.ovpn**, который вы **получили** от администратора:

```bash
openvpn3 config-import --config /path/to/profile.ovpn
```

4. **Включите** VPN:

```bash
openvpn3 session-start --config <profile name>
# profile name - это, обычно, имя импортированного файла, если вы специально не задавали имя профилю
```

Для остальных **сценариев использования** `openvpn3` на **linux** советую ознакомиться с  его **man-страницей** и/или по **ссылкам** ниже:
https://commandmasters.com/commands/openvpn3-linux/
https://www.geekdecoder.com/openvpn-3-client-for-ubuntu-linux/


> Ссылки на другие гайды:
> [Гайд по установке клиентского приложения с официального сайта OpenVPN](https://openvpn.net/cloud-docs/owner/connectors/connector-user-guides/openvpn-3-client-for-linux.html)
> [Неофициальный гайд](https://think.unblog.ch/en/how-to-install-openvpn-client/)

---

## Помощь

Если возникли какие-то **проблемы** - обращайтесь к **администратору/создателю проекта** по **электронной почте**: test@gmail.com
